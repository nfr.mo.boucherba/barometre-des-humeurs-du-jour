import e from "express";
import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class AuthorController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let authorWithQuote : any = []
        let author = db.prepare('SELECT * FROM author').all()
        let check = db.prepare('SELECT * FROM quote').all()
        
        check.forEach((e:any) => {
            if (!authorWithQuote.includes(author[e.author_id-1])){
                authorWithQuote.push(author[e.author_id-1])
            }
        })
        res.render('pages/author', {
            author: authorWithQuote
        });
    }

    static quotes(req: Request, res: Response): void{
        const db = req.app.locals.db;

        let name = req.params
        let authorid = db.prepare('SELECT id FROM author WHERE name = ?').get(name.author)

        if(authorid !==undefined){
            let quotes = db.prepare('SELECT * FROM quote WHERE author_id = ?').all(authorid.id)            

            res.render('pages/authorquote', {
                quote: quotes
            });
        }else{
            let error = [{
                content: "Il n'y a pas d'auteur portant ce nom ici"
            }]
            res.render('pages/authorquote', {
                quote: error
            });
        }
    }
}