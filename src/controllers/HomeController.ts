import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let quote = db.prepare('SELECT * FROM quote').all()
        let thequote = quote[quote.length-1]

        let author = db.prepare('SELECT name FROM author WHERE id = ?').get(thequote.author_id)

        res.render('pages/index', {
            quote: thequote,
            author: author,
        });
    }
}