import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class PanelController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        res.render('pages/index', {
        });
    }
}