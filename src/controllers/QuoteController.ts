import e from "express";
import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class QuoteController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let allquote : any = []
        let quote = db.prepare('SELECT * FROM quote').all()
        let author = db.prepare('SELECT name FROM author').all()
        quote.forEach((e:any) => {
            allquote.push({
                content: e.content,
                author: author[e.author_id-1].name
            })
        });
        res.render('pages/quote', {
            quote: allquote
        });
    }
}