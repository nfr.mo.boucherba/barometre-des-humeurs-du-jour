BEGIN;
CREATE TABLE "author"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45)
);
CREATE TABLE "admin"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45),
  "password" VARCHAR(45)
);
CREATE TABLE "quote"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "author_id" INTEGER,
  "content" VARCHAR(45),
  CONSTRAINT "fk_quote_author1"
    FOREIGN KEY("author_id")
    REFERENCES "author"("id")
);
CREATE INDEX "quote.fk_quote_author1_idx" ON "quote" ("author_id");
COMMIT;
