import { Application } from "express";
import HomeController from "./controllers/HomeController";
import QuoteController from "./controllers/QuoteController";
import AuthorController from "./controllers/AuthorController";
import { ParsedQs } from "qs";
//import { redirectLog } from "./server";
import { Request, ParamsDictionary, Response } from "express-serve-static-core";
import { body, validationResult } from 'express-validator';
import PanelController from "./controllers/PanelController";


export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/quote', (req, res) =>
    {
        QuoteController.index(req, res);
    });

    app.get('/author', (req, res) =>
    {
        AuthorController.index(req, res);
    })

    app.get('/:author', (req, res) =>
    {
        req.params.author
        AuthorController.quotes(req, res);
    })
    app.get('/panel', (req, res) =>
    {
        PanelController.index(req, res);
    })
}




